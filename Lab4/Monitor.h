#ifndef MONITOR_H
#define MONITOR_H

#include <Device.h>


class Monitor : public Device
{
    public:
        Monitor();

        Resolution GetmaxResolution() { return maxResolution; }
        int GetcurFreq() { return curFreq; }
        int GetmaxFreq() { return maxFreq; }

    protected:

    private:
        Resolution maxResolution;
        int curFreq;
        int maxFreq;
};

#endif // MONITOR_H
