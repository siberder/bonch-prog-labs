#ifndef PRINTER_H
#define PRINTER_H

#include <Device.h>


class Printer : public Device
{
    public:
        Printer();

        int GetcurPaperCount() { return curPaperCount; }
        int GetmaxPaperCount() { return maxPaperCount; }
        double GetcurInkAmount() { return curInkAmount; }
        double GetmaxInkAmount() { return maxInkAmount; }

    protected:

    private:
        int curPaperCount;
        int maxPaperCount;
        double curInkAmount;
        double maxInkAmount;
};

#endif // PRINTER_H
