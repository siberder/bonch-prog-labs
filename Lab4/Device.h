#ifndef DEVICE_H
#define DEVICE_H


class Device
{
    public:
        Device();

        double Getprice() { return price; }
        void Setprice(double val) { price = val; }
        double Getcondition() { return condition; }
        void Setcondition(double val) { condition = val; }
        bool GetisPowerOn() { return isPowerOn; }
        void SetisPowerOn(bool val) { isPowerOn = val; }
        bool GetisConnected() { return isConnected; }
        void SetisConnected(bool val) { isConnected = val; }
        string Getfirm() { return firm; }
        void Setfirm(string val) { firm = val; }

    protected:

    private:
        double price;
        double condition;
        bool isPowerOn;
        bool isConnected;
        string firm;
};

#endif // DEVICE_H
