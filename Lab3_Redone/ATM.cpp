#include "ATM.h"

ATM::ATM(std::string _bankName)
{
    bankName = _bankName;
    curMoney = 10000;
    maxMoney = 10000;
}

ATM::ATM(std::string _bankName, double _curMoney, double _maxMoney)
{
    bankName = _bankName;
    curMoney = _curMoney;
    maxMoney = _maxMoney;
}

int ATM::withdrawMoney(double amount)
{
    if (hasBankCard)
    {
        if (curMoney - amount >= 0)
        {
            if(currentCard.subMoney(amount))
            {
                curMoney -= amount;
                return 0;
            }
            else
            {
                return 3;
            }
        }
        else
        {
            return 2;
        }
    }
    else
    {
        return 1;
    }
}

int ATM::addMoney(double amount)
{
    if (hasBankCard)
    {
        if (curMoney + amount <= maxMoney)
        {
            bool canAdd = currentCard.addMoney(amount);
            if (canAdd)
            {
                curMoney += amount;
                return 0;
            }
            else
            {
                return 3;
            }
        }
        else
        {
            return 2;
        }
    }
    else
    {
        return 1;
    }

}

bool ATM::fillMoney(double amount)
{
    if (curMoney > 0 && curMoney + amount <= maxMoney)
    {
        curMoney += amount;
        return true;
    }
    else
    {
        return false;
    }
}

bool ATM::insertCard(BankCard card)
{
    if (!hasBankCard && !card.IsEmpty())
    {
        currentCard = card;
        hasBankCard = true;
        return true;
    }
    else
    {
        return false;
    }
}

BankCard ATM::returnCard()
{
    if (hasBankCard)
    {
        return currentCard;
        hasBankCard = false;
    }
    else
    {
        BankCard emptyCard;
        return emptyCard;
    }
}
