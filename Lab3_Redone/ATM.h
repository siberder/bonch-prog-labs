#ifndef ATM_H
#define ATM_H

#include <string>
#include "BankCard.h"

class ATM
{
    public:
        ATM(std::string _bankName);
        ATM(std::string _bankName, double _curMoney, double _maxMoney);

        std::string GetbankName() { return bankName; }
        void SetbankName(std::string val) { bankName = val; }
        double GetcurMoney() { return curMoney; }
        double GetmaxMoney() { return maxMoney; }
        void SetmaxMoney(double val) { maxMoney = val; }

        int withdrawMoney(double amount);
        int addMoney(double amount);
        bool fillMoney(double amount);

        bool HasBankCard() { return hasBankCard; }
        bool insertCard(BankCard card);
        BankCard GetCurrentCard() { return currentCard; }
        BankCard returnCard();

    protected:

    private:
        std::string bankName;
        double maxMoney;
        double curMoney;
        bool hasBankCard;
        BankCard currentCard;
};

#endif // ATM_H
