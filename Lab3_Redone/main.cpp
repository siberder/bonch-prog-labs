#include <iostream>
#include "BankCard.h"
#include "ATM.h"
#include "windows.h"

using namespace std;

bool showAtmMachineInterface(ATM& machine)
{
    system("cls");

    if (!machine.HasBankCard())
    {
        cout << "Welcome to " << machine.GetbankName() << "!" << endl << endl;
        cout << "Please, insert your card, or move out." << endl << endl;
    }
    else
    {
        cout << "Welcome to " << machine.GetbankName() << ", " << machine.GetCurrentCard().GetHolder() << "!" << endl;
        cout << "You have " << machine.GetCurrentCard().GetMoney() << " on your account." << endl;

        cout << "ATM Money: " << machine.GetcurMoney() << " ATM Max Money: " << machine.GetmaxMoney() << endl << endl;

        cout << "Choose what you want to do:" << endl;
        cout << "1. Add money" << endl;
        cout << "2. Withdraw money" << endl;
        cout << "3. Fill ME with MoNeY" << endl;
        cout << "4. Return my card and exit" << endl;
        cout << " > ";

        int choice;
        cin >> choice;

        if(cin.fail())
        {
            return false;
        }

        double tmp;
        int returnStatus;

        switch (choice)
        {
        case 1:
            cout << "How much money do you want to add to your bank account?" << endl;
            cout << "ATM add limit: " << machine.GetmaxMoney() - machine.GetcurMoney() << endl;
            cout << " > ";
            cin >> tmp;

            returnStatus = machine.addMoney(tmp);

            switch (returnStatus)
            {
            case 0:
                cout << "Successfully added money to your account!" << endl;
                cout << "You have " << machine.GetCurrentCard().GetMoney() << " on your account." << endl;
                break;
            case 1:
                cout << "Cant execute operation, please, insert your card first." << endl;
                break;
            case 2:
                cout << "Cant add money to your account, it's too much for me >_<" << endl;
                break;
            case 3:
                cout << "Cant add money to your account, it's inappropriate value." << endl;
                break;
            }

            break;

        case 2:
            cout << "How much money do you want to withdraw from your bank account?" << endl;
            cout << "ATM withdraw limit: " << machine.GetcurMoney() << endl;
            cout << " > ";
            cin >> tmp;

            returnStatus = machine.withdrawMoney(tmp);

            switch(returnStatus)
            {
            case 0:
                cout << "Please, take your card" << endl;
                machine.returnCard();
                system("pause");
                cout << "Please, take your money" << endl;
                cout << "[" << tmp << " in cash amount is waiting for you in cash dispenser, take them!]" << endl;
                system("pause");
                cout << "Thanks for visit!" << endl;
                Sleep(1000);
                return false;
            case 1:
                cout << "Cant execute operation, please, insert your card first." << endl;
                break;
            case 2:
                cout << "Cant withdraw money from your account, reached withdraw limit." << endl;
                break;
            case 3:
                cout << "Cant withdraw money from your account, insufficient funds on account." << endl;
                break;
            }
            break;

        case 3:
            cout << "How much money do you want to add to this ATM?" << endl;
            cout << "ATM add limit: " << machine.GetmaxMoney() - machine.GetcurMoney() << endl;
            cout << " > ";
            cin >> tmp;

            if (machine.fillMoney(tmp))
            {
                cout << "Successfully filled ATM with money!" << endl;
            }
            else
            {
                cout << "Cant add money, it's too much for me >_<" << endl;
            }

            break;

        case 4:
            cout << "Please, take your card" << endl;
            machine.returnCard();
            system("pause");
            cout << "Thanks for visit!" << endl;
            Sleep(1000);
            return false;
        }
    }

    system("pause");
    return true;
}

int main()
{
    // Creating ATM from bank "MyBank"
    ATM atmMachine("MyBank", 50000, 100000);

    // Creating bank card
    BankCard card("Name Surname", 30000);

    // Trying to open ATM interface without card inserted
    showAtmMachineInterface(atmMachine);

    while(true)
    {
        cout << "Inserting card to ATM machine..";
        Sleep(1500);

        // Inserting card to ATM machine
        atmMachine.insertCard(card);

        // Showing interface with card
        while(showAtmMachineInterface(atmMachine));
    }

    return 0;
}
