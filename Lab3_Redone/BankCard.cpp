#include "BankCard.h"

BankCard::BankCard()
{
    isEmpty = true;
    holder = "";
    money = 0;
}

BankCard::BankCard(std::string _holder)
{
    isEmpty = false;
    holder = _holder;
    money = 0;
}

BankCard::BankCard(std::string _holder, double _money)
{
    isEmpty = false;
    holder = _holder;
    money = _money;
}

bool BankCard::addMoney(double amount)
{
    if (!isEmpty && amount > 0)
    {
        money += amount;
        return true;
    }
    else
    {
        return false;
    }
}

bool BankCard::subMoney(double amount)
{
    if (!isEmpty && amount > 0 && money - amount >= 0)
    {
        money -= amount;
        return true;
    }
    else
    {
        return false;
    }
}
