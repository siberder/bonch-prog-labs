#ifndef BANKCARD_H
#define BANKCARD_H

#include <string>

class BankCard
{
    public:
        BankCard();
        BankCard(std::string _holder);
        BankCard(std::string _holder, double _money);

        std::string GetHolder() { return holder; }
        double GetMoney() { return money; }

        bool addMoney(double amount);
        bool subMoney(double amount);
        bool IsEmpty() { return isEmpty; }

    protected:

    private:
        bool isEmpty;
        std::string holder;
        double money;
};

#endif // BANKCARD_H
