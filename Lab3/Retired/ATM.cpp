#include "ATM.h"

ATM::ATM(Bank bank)
{
    currentBank = bank;
    curMoney = 100000;
    maxMoney = 100000;
    maxMoneyPerWithdraw = 15000;
}

ATM::ATM(Bank bank, double _curMoney, double _maxMoney, double _maxMoneyPerWithdraw)
{
    currentBank = bank;
    curMoney = _curMoney;
    maxMoney = _maxMoney;
    maxMoneyPerWithdraw = _maxMoneyPerWithdraw;
}

bool ATM::withdrawMoney(int amount)
{
    if (curMoney >= amount)
    {
        curMoney -= amount;
        return true;
    }
    else
    {
        return false;
    }
}


bool ATM::insertCard(BankCard card)
{
    BankAccount account = currentBank.getAccount(card.getAccountID());

    if (account.getID() < 0)
    {
        return false;
    }
    else
    {
        currentAccount = account;
        return true;
    }
}

bool ATM::logout()
{

}
