#ifndef BANK_H
#define BANK_H

#include "BankAccount.h"
#include "BankCard.h"
#include "vector"

class Bank
{
    public:
        Bank();
        Bank(std::string);

        std::string GetbankName() { return bankName; }
        void SetbankName(std::string val) { bankName = val; }

        BankAccount getAccount (int accountID);
        BankCard addAccount (std::string holder);

        bool requestMoneySubstract(BankAccount account, double moneyAmount);
        bool requestMoneyAdd(BankAccount account, double moneyAmount);

    private:
        std::string bankName;
        std::vector<BankAccount> accounts;
};

#endif // BANK_H
