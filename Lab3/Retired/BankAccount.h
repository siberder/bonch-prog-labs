#ifndef BANKACCOUNT_H
#define BANKACCOUNT_H

#include "string"

struct BankAccount
{
    public:
        BankAccount() { id = -1; }
        BankAccount(int _id, std::string _holder, double _money)
        {
            id = _id;
            holder = _holder;
            money = _money;
        }

        std::string getHolder() { return holder; }
        void setHolder(std::string val) { holder = val; }
        double getMoney() { return money; }
        void setMoney(double val) { money = val; }
        int getID() { return id; }

    private:
        int id;
        std::string holder;
        double money;
};

#endif
