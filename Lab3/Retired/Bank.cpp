#include "BankAccount.h"
#include "Bank.h"

#include <vector>

Bank::Bank ()
{
    bankName = "Undefined";
}

Bank::Bank (std::string _bankName)
{
    bankName = _bankName;
}

BankAccount Bank::getAccount (int accountID)
{
    for(int i = 0; i < accounts.size(); i++)
    {
        if (accounts[i].getID() == accountID)
        {
            return accounts[i];
        }
    }

    BankAccount nullAcc;
    return nullAcc;
}

BankCard Bank::addAccount (std::string holder)
{
    int lastID = -1;

    if(accounts.size() > 0)
    {
        lastID = accounts.back().getID();
    }

    BankAccount account(lastID + 1, holder, 0);
    accounts.push_back(account);

    BankCard card(lastID);

    return card;
}

bool Bank::requestMoneySubstract(BankAccount account, double moneyAmount)
{
    return true;
}

bool Bank::requestMoneyAdd(BankAccount account, double moneyAmount)
{
    return false;
}
