#ifndef ATM_H
#define ATM_H

#include "BankAccount.h"
#include "BankCard.h"
#include "Bank.h"

class ATM
{
    public:
        ATM(Bank bank);
        ATM(Bank bank, double curMoney, double maxMoney, double maxMoneyPerWithdraw);

        bool GetisPowerOn() { return isPowerOn; }
        void SetisPowerOn(bool val) { isPowerOn = val; }
        double GetcurMoney() { return curMoney; }
        void SetcurMoney(double val) { curMoney = val; }
        double GetmaxMoney() { return maxMoney; }
        void SetmaxMoney(double val) { maxMoney = val; }
        double GetmaxMoneyPerWithdraw() { return maxMoneyPerWithdraw; }
        void SetmaxMoneyPerWithdraw(double val) { maxMoneyPerWithdraw = val; }

        bool withdrawMoney(int amount);
        bool addMoney(int amount);

        void insertCard(BankCard card);
        void logout();

    protected:

    private:
        bool isPowerOn;
        double curMoney;
        double maxMoney;
        double maxMoneyPerWithdraw;
        Bank currentBank;
        BankAccount currentAccount;
};

#endif // ATM_H
