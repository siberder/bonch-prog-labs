#ifndef BANKCARD_H
#define BANKCARD_H

#include "string"

struct BankCard
{
    public:
        BankCard(int accID);
        int getAccountID() { return accountID; }

    private:
        int accountID;
};

#endif

