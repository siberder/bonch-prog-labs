#ifndef ATM_H
#define ATM_H


class ATM
{
    public:
        ATM();

        bool GetisPowerOn() { return isPowerOn; }
        void SetisPowerOn(bool val) { isPowerOn = val; }
        double GetmaxMoney() { return maxMoney; }
        void SetmaxMoney(double val) { maxMoney = val; }
        double GetcurMoney() { return curMoney; }
        void SetcurMoney(double val) { curMoney = val; }
        double GetmaxMoneyPerWithdraw() { return maxMoneyPerWithdraw; }
        void SetmaxMoneyPerWithdraw(double val) { maxMoneyPerWithdraw = val; }

    protected:

    private:
        bool isPowerOn;
        double maxMoney;
        double curMoney;
        double maxMoneyPerWithdraw;
};

#endif // ATM_H
