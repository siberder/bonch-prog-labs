#ifndef MONEYBANK_H
#define MONEYBANK_H

#include "string"

class MoneyBank
{
    public:
        MoneyBank();

        std::string GetbankName() { return bankName; }
        void SetbankName (std::string val) { bankName = val; }

    protected:

    private:
        std::string bankName;
};

#endif // MONEYBANK_H
