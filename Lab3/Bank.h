#ifndef BANK_H
#define BANK_H

#include "string"

class Bank
{
    public:
        Bank();

        std::string GetbankName() { return bankName; }
        void SetbankName(std::string val) { bankName = val; }

    protected:

    private:
        std::string bankName;
};

#endif // BANK_H
