#ifndef RESOULTION_H
#define RESOULTION_H


class Resoultion
{
public:
    Resoultion();
    Resoultion(unsigned int _width, unsigned int _height);
    virtual ~Resoultion();

    unsigned int Getwidth()
    {
        return width;
    }

    void Setwidth(unsigned int val)
    {
        width = val;
    }

    unsigned int Getheight()
    {
        return height;
    }

    void Setheight( unsigned int val)
    {
        height = val;
    }

protected:

private:
    unsigned int width;
    unsigned int height;
};

#endif // RESOULTION_H
