#ifndef MONITOR_H
#define MONITOR_H

#include <string>
#include "Resoultion.h"

class Monitor
{
public:
    Monitor();
    Monitor(std::string _model, Resoultion _maxResolution, double _price);
    virtual ~Monitor();

    std::string Getmodel()
    {
        return model;
    }

    void Setmodel(std::string val)
    {
        if(val.length() > 0)
        {
            model = val;
        }
    }

    Resoultion GetmaxResolution()
    {
        return maxResolution;
    }

    void SetmaxResolution(Resoultion val)
    {
        maxResolution = val;
    }

    double Getprice()
    {
        return price;
    }

    void Setprice(double val)
    {
        if (val > 0)
        {
            price = val;
        }
    }

protected:

private:
    std::string model;
    Resoultion maxResolution;
    double price;
};

#endif // MONITOR_H
