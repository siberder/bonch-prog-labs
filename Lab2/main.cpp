#include <iostream>
#include <fstream>
#include <windows.h>
#include <string>
#include "Monitor.h"
#include "Resoultion.h"

using namespace std;

const int rowsCount = 4;

string filePath = "monitors.txt";

Monitor monitors[rowsCount];

bool loadMonitorsFromFile()
{
    ifstream inFile;
    inFile.open(filePath);

    if (!inFile)
    {
        return false;
    }

    string model;
    unsigned int width, height;
    double price;

    for (int i = 0; i < rowsCount; i++)
    {
        inFile >> model;
        inFile >> width;
        inFile >> height;
        inFile >> price;

        Resoultion res(width, height);
        monitors[i] = Monitor(model, res, price);
    }

    inFile.close();

    return true;
}

void saveMonitorsToFile()
{
    cout << "Enter new file path: ";

    string newFilePath;
    cin >> newFilePath;

    ofstream outFile;
    outFile.open(newFilePath);

    for (int i = 0; i < rowsCount; i++)
    {
        outFile << monitors[i].Getmodel() << " ";
        outFile << monitors[i].GetmaxResolution().Getwidth() << " ";
        outFile << monitors[i].GetmaxResolution().Getheight() << " ";
        outFile << monitors[i].Getprice() << " ";
        outFile << endl;
    }

    outFile.close();

    cout << "Monitors saved to " << newFilePath << endl;
}

double countAverageMonitorsPrice()
{
    double sum = 0;
    double averagePrice = 0;

    for (int i = 0; i < rowsCount; i++)
    {
        sum += monitors[i].Getprice();
    }

    averagePrice = sum / rowsCount;

    return averagePrice;
}

void enterRowEditMode()
{
    cout << "Enter monitor row: ";

    int row;
    cin >> row;

    row--;

    if(row < 0 || row >= rowsCount)
    {
        cout << "Invalid row number!" << endl;
        system("pause");
        return;
    }

    string model;
    unsigned int width, height;
    double price;

    Monitor mon = monitors[row];
    cout << "You editing monitor #" << row + 1 << endl;

    cout << "Enter monitor model (" << mon.Getmodel() << "): ";
    cin >> model;
    mon.Setmodel(model);

    Resoultion res = mon.GetmaxResolution();

    cout << "Enter monitor resolution width (" << res.Getwidth() << "): ";
    cin >> width;
    res.Setwidth(width);

    cout << "Enter monitor resolution height (" << res.Getheight() << "): ";
    cin >> height;
    res.Setheight(height);

    mon.SetmaxResolution(res);

    cout << "Enter monitor price (" << mon.Getprice() << "): ";
    cin >> price;
    mon.Setprice(price);

    monitors[row] = mon;
}

void showCurrentMonitorsArray()
{
    cout << "Monitors:" << endl;

    cout << "# ";
    cout << "MODEL ";
    cout << "RESOLUTION ";
    cout << "PRICE ";;
    cout << endl;

    for (int i = 0; i < rowsCount; i++)
    {
        cout << i + 1 << " ";
        cout << monitors[i].Getmodel() << " ";
        cout << monitors[i].GetmaxResolution().Getwidth() << "x";
        cout << monitors[i].GetmaxResolution().Getheight() << " ";
        cout << monitors[i].Getprice();
        cout << endl;
    }

    cout << endl;
}

bool menu()
{
    system("cls");

    showCurrentMonitorsArray();

    cout << "What do you want to do?" << endl;
    cout << "1. Edit monitors row" << endl;
    cout << "2. Save monitors" << endl;
    cout << "3. Count average monitors price" << endl;
    cout << "4. Exit" << endl;
    cout << endl << ">> ";

    int option;
    cin >> option;

    if(cin.fail())
    {
        return false;
    }

    switch(option)
    {
    case 1:
        enterRowEditMode();
        return true;

    case 2:
        saveMonitorsToFile();
        system("pause");

        return false;

    case 3:
        cout << "Monitors average price: " << countAverageMonitorsPrice() << endl;
        system("pause");

        return true;

    case 4:
        return false;

    default:
        return true;
    }

    return true;
}

int main()
{
    cout << "Loading monitors from " << filePath << endl;

    if(loadMonitorsFromFile())
    {
        cout << "Monitors loaded!" << endl;
    }
    else
    {
        cout << "Unable to load monitors from file " << filePath << endl;
        system("pause");
        exit(1);
    }

    while(menu()) { }

    exit(1);
}
